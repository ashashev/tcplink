#include "link.h"

namespace
{

bool isConnected(QTcpSocket* socket)
{
    return (socket != nullptr) &&
           (socket->state() == QAbstractSocket::ConnectedState);
}

bool isDisconnected(QTcpSocket* socket)
{
    return (socket != nullptr) &&
           (socket->state() == QAbstractSocket::UnconnectedState);
}

}

namespace TcpLink
{

Link::Link(QTcpSocket *client, QTcpSocket *remote, QObject *parent) :
    QObject(parent),
    client(client),
    remote(remote)
{
    client->setParent(this);
    remote->setParent(this);

    connect( client, SIGNAL(disconnected()), SLOT(onClientDisconnected()) );
    connect( client, SIGNAL(readyRead()), SLOT(onReceivedFromClient()) );

    connect( remote, SIGNAL(disconnected()), SLOT(onRemoteDisconnected()) );
    connect( remote, SIGNAL(readyRead()), SLOT(onReceivedFromRemote()) );

#ifndef WITHOUT_STATISTICS
    statistics.reset();
#endif
}

Link::~Link()
{

}

const Statistics &Link::getStatistics() const
{
    return statistics;
}

void Link::statisticsResetInterval()
{
    statistics.resetInterval();
}

void Link::onClientDisconnected()
{
    if( isConnected(remote) )
    {
        remote->close();
    }

    selfDestroy();
}

void Link::onRemoteDisconnected()
{
    if( isConnected(client) )
    {
        client->close();
    }

    selfDestroy();
}

void Link::onReceivedFromClient()
{
    QByteArray r = client->readAll();
    if (!r.isEmpty() && isConnected(remote))
    {
        remote->write(r);
#ifndef WITHOUT_STATISTICS
        statistics.incFromClient(r.size());
#endif
    }
}

void Link::onReceivedFromRemote()
{
    QByteArray r = remote->readAll();
    if (!r.isEmpty() && isConnected(client))
    {
        client->write(r);
#ifndef WITHOUT_STATISTICS
        statistics.incFromRemote(r.size());
#endif
    }
}

void Link::selfDestroy()
{
    if (isDisconnected(client) && isDisconnected(remote))
    {
        deleteLater();
    }
}

} /* namespace TcpLink */

