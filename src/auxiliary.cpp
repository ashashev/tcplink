#include "auxiliary.h"

#include <iostream>
#include <iomanip>
#include <QtGlobal>

#ifdef Q_OS_WIN
#include <Windows.h>
#endif

#ifdef Q_OS_LINUX
#include <termios.h>
#include <sys/ioctl.h>
#include <unistd.h>
#endif


namespace auxiliary
{

Size getTerminalSize()
{
#ifdef Q_OS_WIN
    auto hconsole = GetStdHandle(STD_OUTPUT_HANDLE);
    if(!hconsole)
    {
        throw UnknownTerminal();
    }

    CONSOLE_SCREEN_BUFFER_INFO info {0};
    BOOL res = GetConsoleScreenBufferInfo(hconsole, &info);
    if(!res)
    {
        throw UnknownTerminal();
    }

    return Size(info.dwSize.X, info.dwSize.Y);
#endif

#ifdef Q_OS_LINUX
    struct winsize size;
    int res = ioctl(STDOUT_FILENO,TIOCGWINSZ,&size);
    if(res != 0)
    {
        throw UnknownTerminal();
    }

    return Size(size.ws_col, size.ws_row);
#endif

    throw UnknownTerminal();
    return Size();
}

void cursorUp(unsigned short lines)
{
#ifdef Q_OS_WIN
    auto hconsole = GetStdHandle(STD_OUTPUT_HANDLE);
    if (!hconsole)
        throw UnknownTerminal();

    CONSOLE_SCREEN_BUFFER_INFO info {0};
    BOOL res = GetConsoleScreenBufferInfo(hconsole, &info);
    if (!res)
        throw UnknownTerminal();

    COORD pos = info.dwCursorPosition;
    pos.Y -= lines;
    pos.X = 0;

    auto resPos = SetConsoleCursorPosition(hconsole, pos);
    if (!resPos)
        throw UnknownTerminal();

    return;
#endif

#ifdef Q_OS_LINUX
    printf("\033[%dA\r", lines);
    return;
#endif

    throw UnknownTerminal();
}

} /* namespace auxiliary */

namespace std
{

using namespace auxiliary;

ostream& operator<< (ostream& os, const ClearLine&)
{
    auto size = getTerminalSize();

    os << "\r" << std::setw(size.x) << " " << "\r";
    os.flush();

#ifdef Q_OS_WIN
    cursorUp(1);
#endif

    return os;
}

ostream& operator<< (ostream& os, const ClearLines& cl)
{
    if (cl.lines == 0)
    {
        os << ClearLine();
        return os;
    }

    auto size = getTerminalSize();
    cursorUp(cl.lines);

    for (unsigned short i = 0; i < cl.lines; ++i)
        os << "\r" << std::setw(size.x - 1) << " " << std::endl;

    cursorUp(cl.lines);

    return os;
}

ostream &operator<<(ostream &os, const Flush &)
{
    os.flush();
    return os;
}

} /* namespace std */
