#include "linker.h"
#include "auxiliary.h"
#include <iostream>
#include <cassert>
#include <iomanip>

namespace
{

const std::string prefixName = "ch";

} /* anonymous namespace */

using namespace auxiliary;

namespace TcpLink
{

Linker::Linker(const Cfg &cfg, QObject *parent) :
    QObject(parent),
    cfg(cfg)
{
    listener = new QTcpServer(this);
    listener->setMaxPendingConnections(20);
    connect( listener, SIGNAL(newConnection()), SLOT(onClientConnected()) );

    if (listener->listen( QHostAddress::AnyIPv4, cfg.listen_port ))
    {
        std::cout << "remote:    " << cfg.remote_addr << ":" << cfg.remote_port << "\n";
        std::cout << "listening: " << listener->serverAddress().toString().toStdString() << ":" << listener->serverPort() << "\n\n";


        std::cout << ClearLine() << "disconnected" << Flush();
    }
    else
    {
        throw std::runtime_error(listener->errorString().toStdString());
    }
}

Linker::~Linker()
{
}

void Linker::onClientConnected()
{
    auto client = listener->nextPendingConnection();
    auto remote = new QTcpSocket(this);
    remote->connectToHost( QHostAddress(cfg.remote_addr.c_str()), cfg.remote_port );

    if( remote->waitForConnected(5000) )
    {
        auto link = new Link(client, remote, this);
        ++last_link_id;
        if (links.empty())
        {
            std::cout << ClearLine() << "connected" << Flush();
#ifndef WITHOUT_STATISTICS
            statistics_timer_id = startTimer(STATISTICS_UPDATE_INTERVAL_MS);
#endif
        }
        std::string name = "ch" + std::to_string(last_link_id);
        links.insert(std::make_pair(link, name));
        connect(link, SIGNAL(destroyed(QObject*)), SLOT(onLinkDestroyed(QObject*)));
    }
    else
    {
        client->abort();
        delete client;
        delete remote;
    }
}

void Linker::onLinkDestroyed(QObject *obj)
{
    auto link = static_cast<Link*>(obj);
    if (link)
    {
        links.erase(link);
    }

#ifndef WITHOUT_STATISTICS
    if (links.empty())
    {
        killTimer(statistics_timer_id);
        statistics_timer_id = 0;
        printStatistics();
        std::cout << ClearLine() << "disconnected" << Flush();
    }
#endif
}

void Linker::timerEvent(QTimerEvent *event)
{
#ifndef WITHOUT_STATISTICS
    if(event->timerId() == statistics_timer_id)
    {
        printStatistics();
    }
#endif
}

void Linker::printStatistics() const
{
#ifndef WITHOUT_STATISTICS
    static unsigned last_lines_count = 0;
    unsigned nameWidth = (prefixName + std::to_string(last_link_id)).size() + 1;
    std::cout << ClearLines(last_lines_count);
    for(auto& link: links)
    {
        std::cout << std::left << std::setw(nameWidth) << link.second << ": "
                  << std::right << Statistics::Summary(link.first->getStatistics())
                  << "\n";
        link.first->statisticsResetInterval();
    }
    std::cout << Flush();
    last_lines_count = links.size();
#endif
}

Linker::Cfg::Cfg() :
    remote_addr("")
  , remote_port(0)
  , listen_port(0)
{}

}//namespace TcpLink
