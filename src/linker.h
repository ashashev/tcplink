#ifndef LINKER_H
#define LINKER_H

#include "statistics.h"
#include "link.h"
#include <QObject>
#include <QtNetwork>
#include <map>

namespace TcpLink
{

class Linker : public QObject
{
    Q_OBJECT
public:
    struct Cfg
    {
        std::string remote_addr;
        unsigned remote_port;
        unsigned listen_port;

        Cfg();
    };

public:
    explicit Linker(const Cfg& cfg, QObject *parent = 0);
    ~Linker();

signals:

private slots:
    void onClientConnected();
    void onLinkDestroyed(QObject* obj);

private:
    void timerEvent(QTimerEvent *event) override;
    void printStatistics() const;

    const Cfg cfg;
    QTcpServer* listener;
    std::map<Link*, std::string> links;
    unsigned last_link_id = 0u;

#ifndef WITHOUT_STATISTICS
    int statistics_timer_id = 0;
#endif
};

}//namespace TcpLink
#endif // LINKER_H
