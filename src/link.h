#ifndef LINK_H
#define LINK_H

#include "statistics.h"
#include <QObject>
#include <QtNetwork>

namespace TcpLink
{

class Link : public QObject
{
    Q_OBJECT
public:
    Link(QTcpSocket* client, QTcpSocket* remote, QObject *parent = nullptr);
    ~Link();

    const Statistics& getStatistics() const;
    void statisticsResetInterval();

signals:

private slots:
    void onClientDisconnected();
    void onRemoteDisconnected();
    void onReceivedFromClient();
    void onReceivedFromRemote();

private:
    void selfDestroy();

    QTcpSocket* client;
    QTcpSocket* remote;

#ifndef WITHOUT_STATISTICS
    Statistics statistics;
#endif
};

} /* namespace TcpLink */

#endif // LINK_H
