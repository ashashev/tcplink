#ifndef AUXILIARY_H
#define AUXILIARY_H

#include <stdexcept>

namespace auxiliary
{

struct ClearLine {};

struct ClearLines
{
    unsigned lines;
    ClearLines(unsigned lines): lines(lines) {}
};

struct Flush {};

struct Size
{
    unsigned short x = 0;
    unsigned short y = 0;

    Size() {}
    Size(unsigned short x, unsigned short y): x(x), y(y) {}
};

struct UnknownTerminal: public std::runtime_error
{
    UnknownTerminal():
        std::runtime_error("Can't get info about terminal")
    {}
};

Size getTerminalSize();
void cursorUp(unsigned short lines);

} /* namespace auxiliary */

namespace std
{

ostream& operator<< (ostream& os, const auxiliary::ClearLine&);
ostream& operator<< (ostream& os, const auxiliary::ClearLines& cl);
ostream& operator<< (ostream& os, const auxiliary::Flush&);

} /* namespace std */

#endif // AUXILIARY_H
