#ifndef STATISTICS_H
#define STATISTICS_H

#include <cstdint>
#include <iostream>
#include <chrono>

namespace TcpLink
{

class Statistics
{
public:
    struct Summary
    {
        const Statistics& info;
        const int width;
        Summary(const Statistics& info, int width = 15);
    };

    Statistics();

    const std::uintmax_t& fromClient() const;
    const std::uintmax_t& fromRemote() const;

    void resetInterval();
    const std::uintmax_t& fromClientInterval() const;
    const std::uintmax_t& fromRemoteInterval() const;

    using Interval = std::chrono::milliseconds;
    const Interval interval() const;

    void reset();

    void incFromClient(std::uintmax_t inc);
    void incFromRemote(std::uintmax_t inc);
private:
    using Clock = std::chrono::high_resolution_clock;
    using TimePoint = Clock::time_point;

    std::uintmax_t from_client = 0u;
    std::uintmax_t from_remote = 0u;

    std::uintmax_t from_client_interval = 0u;
    std::uintmax_t from_remote_interval = 0u;

    TimePoint last_reset;
    TimePoint last_update;
};

} /* namespace TcpLink */

namespace std
{

ostream& operator<< (ostream& os, const TcpLink::Statistics& value);
ostream& operator<< (ostream& os, const TcpLink::Statistics::Summary& value);

}

#endif // STATISTICS_H
