#include <QCoreApplication>

#include <iostream>

#include "linker.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    TcpLink::Linker::Cfg cfg;

    for( int p = 1; p < argc; ++p )
    {
        if( strcmp( argv[p], "-h" ) == 0 || strcmp( argv[p], "--help" ) == 0)
        {
            std::cout << "Options:\r\n"
                      << "-s <remote ip address>\r\n"
                      << "-p <remote port>\r\n"
                      << "-lp <listen port>\r\n" << std::endl;
            return 0;
        }
        else if( strcmp( argv[p], "-p" ) == 0 )
        {
            if( ++p < argc )
                cfg.remote_port = atoi( argv[p] );
        }
        else if( strcmp( argv[p], "-s" ) == 0 )
        {
            if( ++p < argc )
                cfg.remote_addr = argv[p];
        }
        else if( strcmp( argv[p], "-lp" ) == 0 )
        {
            if( ++p < argc )
                cfg.listen_port = atoi( argv[p] );
        }
    }

    try
    {
        TcpLink::Linker linker(cfg);
        return a.exec();
    }
    catch(std::runtime_error& err)
    {
        std::cerr << err.what() << std::endl;
        return 1;
    }
}
