#-------------------------------------------------
#
# Project created by QtCreator 2014-04-17T18:50:07
#
#-------------------------------------------------

QT       += core network

QT       -= gui

TARGET = tcplink
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    linker.cpp \
    statistics.cpp \
    link.cpp \
    auxiliary.cpp

HEADERS += \
    linker.h \
    statistics.h \
    link.h \
    auxiliary.h

DEFINES += STATISTICS_UPDATE_INTERVAL_MS=1000
