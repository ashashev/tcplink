#include "statistics.h"
#include <limits>
#include <string>
#include <utility>
#include <map>
#include <functional>
#include <algorithm>
#include <iomanip>
#include <sstream>
#include <cmath>

namespace
{

void safeInc(std::uintmax_t& value, const std::uintmax_t& inc)
{
    static const auto max = std::numeric_limits<std::uintmax_t>::max();
    auto remainder = max - value;

    if (remainder > inc)
    {
        value += inc;
    }
    else
    {
        value = inc - remainder;
    }
}

const std::uintmax_t bytes = 1;
const std::uintmax_t kiloBytes = 1024 * bytes;
const std::uintmax_t megaBytes = 1024 * kiloBytes;
const std::uintmax_t gigaBytes = 1024 * megaBytes;

std::pair<std::uintmax_t, std::string> findUnits(const std::uintmax_t& value)
{
    using UnitsMap = std::map<std::uintmax_t, std::string, std::greater<std::uintmax_t> >;
    static const UnitsMap units = {
        {gigaBytes, "GB"},
        {megaBytes, "MB"},
        {kiloBytes, "kB"}
    };
    for(const auto& unit: units)
    {
        if (unit.first < value)
            return unit;
    }
    return std::make_pair(bytes, std::string("B"));
}

std::pair<std::uintmax_t, std::string> shortestFormat(const std::uintmax_t& value)
{
    auto p = findUnits(value);
    p.first = value / p.first;
    return p;
}

std::string to_string(const std::pair<uintmax_t, std::string>& v)
{
    static std::stringstream ss;
    ss.str("");
    ss << v.first << " " << std::setw(2) << v.second;
    return ss.str();
}

using Seconds = std::chrono::duration<double>;

std::pair<double, std::string> shortestFormat(const std::uintmax_t& flow, const Seconds interval)
{
    const auto speed = interval.count() > 0? flow / interval.count(): 0.0;
    const auto unit = findUnits(std::llround(std::floor(speed)));
    const std::string speed_unit = unit.second + "/" + "s";
    const auto speed_in_units = std::round(speed * 10.0 / unit.first) / 10.0;
    return std::make_pair(speed_in_units, speed_unit);
}

std::string to_string(const std::pair<double, std::string>& v)
{
    static std::stringstream ss;
    ss.str("");
    ss << std::fixed << std::setprecision(1) << v.first << " "
       << std::setw(4) << v.second;
    return ss.str();
}

} /* anoynmous namespace */

namespace TcpLink
{

Statistics::Statistics():
    last_reset(Clock::now()),
    last_update(last_reset)
{

}

const uintmax_t& Statistics::fromClient() const
{
    return from_client;
}

const uintmax_t& Statistics::fromRemote() const
{
    return from_remote;
}

void Statistics::resetInterval()
{
    from_client_interval = 0u;
    from_remote_interval = 0u;

    last_reset = Clock::now();
    last_update = last_reset;
}

const std::uintmax_t& Statistics::fromClientInterval() const
{
    return from_client_interval;
}

const std::uintmax_t& Statistics::fromRemoteInterval() const
{
    return from_remote_interval;
}

const Statistics::Interval Statistics::interval() const
{
    auto diff = last_update - last_reset;
    return std::chrono::duration_cast<Interval>(last_update - last_reset);
}

void Statistics::reset()
{
    from_client = 0u;
    from_remote = 0u;
    resetInterval();
}

void Statistics::incFromClient(uintmax_t inc)
{
    last_update = Clock::now();
    safeInc(from_client, inc);
    safeInc(from_client_interval, inc);
}

void Statistics::incFromRemote(uintmax_t inc)
{
    last_update = Clock::now();
    safeInc(from_remote, inc);
    safeInc(from_remote_interval, inc);
}

Statistics::Summary::Summary(const Statistics &info, int width):
    info(info),
    width(width)
{

}

} /* namespace TcpLink */

std::ostream &std::operator<<(std::ostream &os, const TcpLink::Statistics &value)
{
    const auto client = shortestFormat(value.fromClient());
    const auto remote = shortestFormat(value.fromRemote());

    const auto interval = std::chrono::duration_cast<Seconds>(value.interval());
    const auto clientSpeed = shortestFormat(value.fromClientInterval(), interval);
    const auto remoteSpeed = shortestFormat(value.fromRemoteInterval(), interval);

    os << "client: "
       << std::fixed << std::setprecision(1)
       << clientSpeed.first << " " << clientSpeed.second
       << " (" << client.first << " " << client.second << ")"
       << " remote: "
       << remoteSpeed.first << " " << remoteSpeed.second
       << " (" << remote.first << " " << remote.second << ")";
    return os;
}

std::ostream& std::operator<< (std::ostream& os, const TcpLink::Statistics::Summary& params)
{
    const TcpLink::Statistics& value = params.info;
    const auto total = shortestFormat(value.fromClient() + value.fromRemote());
    const auto interval = std::chrono::duration_cast<Seconds>(value.interval());
    const auto totalSpeed = shortestFormat(
                value.fromClientInterval() + value.fromRemoteInterval(),
                interval
            );

    os << std::setw(params.width) << ::to_string(totalSpeed)
       << std::setw(params.width) << ::to_string(total);

    return os;
}
